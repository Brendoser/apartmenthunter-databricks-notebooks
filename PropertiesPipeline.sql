-- Databricks notebook source
-- MAGIC %md
-- MAGIC # Silver Layer

-- COMMAND ----------

CREATE OR REPLACE LIVE TABLE properties_silver
COMMENT "Only including the most informative columns from the rental listings"
AS SELECT
  bedrooms,
  community,
  city,
  location,
  lease_term,
  latitude,
  longitude,
  garage_size,
  features,
  utilities_included,
  price,
  dogs,
  sq_feet,
  province,
  baths,
  type,
  cats
FROM default.properties;


-- COMMAND ----------

-- MAGIC %md
-- MAGIC # Gold Layer

-- COMMAND ----------

CREATE OR REFRESH LIVE TABLE properties_gold
COMMENT "Doing some aggregations and stuff..."
SELECT * FROM live.properties_silver
WHERE bedrooms NOT LIKE '%-%' and price NOT LIKE '%-%' where bedrooms not like 'studio' and bedrooms not like '%-%';
